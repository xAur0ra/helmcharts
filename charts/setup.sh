#!/bin/bash

/home/core/custom_bin/linux-amd64/helm repo add bitnami https://charts.bitnami.com/bitnami 


sleep 5

cd /home/core/custom_bin/helmcharts-main/charts/cloud-csi/ && kubectl create secret -n kube-system generic cloud-config --from-file=cloud.conf

git clone https://github.com/kubernetes/cloud-provider-openstack && cd cloud-provider-openstack

git checkout tags/v1.27.3

rm manifests/cinder-csi-plugin/csi-secret-cinderplugin.yaml

kubectl -f manifests/cinder-csi-plugin/ apply

cd

cd /home/core/custom_bin/helmcharts-main/charts/ && kubectl apply -f sc.yaml

cd /home/core/custom_bin/helmcharts-main/charts/ && kubectl apply -f mongodb-secret.yaml

cd /home/core/custom_bin/helmcharts-main/charts/ && kubectl apply -f redis-secret.yaml 

cd /home/core/custom_bin/helmcharts-main/charts/ && kubectl apply -f rabbitmq-secret.yaml



/home/core/custom_bin/linux-amd64/helm dependency build /home/core/custom_bin/helmcharts-main/charts/corpool 

/home/core/custom_bin/linux-amd64/helm upgrade --install corpool /home/core/custom_bin/helmcharts-main/charts/corpool



